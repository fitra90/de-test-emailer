const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema({
    first_name: {
        required: true,
        type: String
    },
    last_name: {
        required: true,
        type: String
    },
    email: {
        required: true,
        type: String
    },
    phone: {
        required: false,
        type: String
    },
    address: {
        required: false,
        type: String
    },
    birth_date: {
        required: true,
        type: Date
    },
    join_date: {
        required: true,
        type: Date
    },
    timezone: {
        required: true,
        type: String
    }
})

module.exports = mongoose.model('Data', dataSchema)