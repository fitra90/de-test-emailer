require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
const mongoString = process.env.DATABASE_URL;
const routes = require('./routes/routes');
const cron = require('node-cron');
const User = require('./models/User');
const FailedMessage = require('./models/FailedMessage')
const axios = require('axios');

const app = express();

mongoose.connect(mongoString);
const database = mongoose.connection;

database.on('error', error=>{
  console.log(error);
});

database.once('connected', ()=>{
  console.log('Database Connected')
});

app.use(express.json());
app.use('/api', routes);

//cron job to check birthday every hour
// cron.schedule('0 * * * *',  function() {
  cron.schedule('* * * * *',  function() {
  console.log("running a task every 1 minute");
  
  //get user 
  // const userOnBirthday = async()=>{
    //IF TODAY 9:00 (MOMENTJS TIMEZONE) = USER'S BIRTHDAY (MOMENTJS TIMEZONE)
    //SEND EMAIL 
    if(true) {
        sendEmail('FItra', 'Fadilana', 'fitra90@gmasdf.com', 'Happy Birthday!');
    }
    
  // }

});

async function sendEmail(first_name, last_name, email, messages) {
  try {
    let name = first_name + ' ' + last_name;
    let bMessages = messages + ',' + name;
    let payload = { email: email, message: bMessages };

    let res = await axios.post(process.env.EMAIL_SERVICE, payload);

    let data = res.data;
    console.log(data);

  } catch (error) {
    console.log(error)
    
    //collect save failed message to
  }
    
 
}

app.use((err, req, res, next) => {
    const statusCode = err.statusCode || 500;
    console.error(err.message, err.stack);
    res.status(statusCode).json({ message: err.message });
    return;
  });

const PORT = process.env.PORT;

app.listen(PORT, ()=> console.log(`Server stareted on port ${PORT}`));

