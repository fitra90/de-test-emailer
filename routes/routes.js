const express = require('express');

const router = express.Router()
const User = require('../models/User');

//CREATE MEMBER
router.post('/post', async (req, res)=> {
    const data = new User({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        phone: req.body.phone,
        address: req.body.address,
        birth_date: req.body.birth_date,
        join_date: req.body.join_date,
        timezone: req.body.timezone,
    });
    try {

        const dataToSave = await data.save();
        res.status(200).json(dataToSave);
    } 
    catch(error) {
        res.status(400).json({message: error.message});
    }
});

//GET ALL MEMBER
router.get('/getAll', async (req, res)=>{
    try{
        const data = await User.find();
        res.json(data)
    }
    catch(error){
        res.status(500).json({message: error.message})
    }
});

//UPDATE BY ID
router.patch('/update/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const updatedData = req.body;
        const options = { new: true };

        const result = await User.findByIdAndUpdate(
            id, updatedData, options
        )

        res.send(result)
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
})
//DELETE BY ID
router.delete('/delete/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const data = await User.findByIdAndDelete(id)
        res.send(`Document id ${data._id} has been deleted..`)
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
})


module.exports = router;